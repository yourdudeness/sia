import $ from 'jquery';
import Inputmask from 'inputmask';

let forms = {

  init: function() {
    forms.app = this;

    this.document.ready(() => {
      forms.initMask();
    });
  },

  initMask() {
    let selector = document.querySelectorAll('.mask-validate');
    let betMask = document.querySelector('.js-input-bet');
    Inputmask({
      mask: '+7 (999) 999 99 99',
      showMaskOnHover: false,
    }).mask(selector);

    // Inputmask({
    //   mask: '999 999 999 999 Р',
    //   showMaskOnHover: false,
    //   numericInput: true,
    //   inputFormat:'999 999 999',
    //   inputmode:'decimal'
    // }).mask(betMask);

    Inputmask("(999){+|1}P", {
      positionCaretOnClick: "radixFocus",
      radixPoint: ",",
      numericInput: true,
      placeholder: "0",
      definitions: {
        "0": {
          validator: "[0-9\uFF11-\uFF19]"
        }
      }
    }).mask(betMask);
  },

};

export default forms;