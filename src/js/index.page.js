import 'jquery.global.js';
import Marquee3k from 'marquee3000';
import LocomotiveScroll from 'locomotive-scroll';

let mainPage = {

  init: function() {
    mainPage.app = this;

    this.document.ready(() => {
      mainPage.initIndex();
    });
  },

  initIndex() {
    Marquee3k.init();
    var scroll = new LocomotiveScroll({
      el: document.querySelector('[data-scroll-container]'),
      smooth: true,
      reloadOnContextChange: true,
    });

    window.addEventListener('resize', function() {

      if (window.innerWidth < 681) {
        scroll.destroy();
      }

    });

    if (window.innerWidth < 681) {
      scroll.destroy();
    }


    let test = document.querySelectorAll('.singer-profile');
    test.forEach((item, index) => {
      let counter = item.querySelector('.singer-position p');
      let counteText = '0' + (index + 1);
      counter.innerHTML = counteText.substr(-2, 2);

    })
  }

};

export default mainPage;