import 'jquery.global.js';
//import 'lazysizes';
import _ from 'lodash';

import fancybox from '@fancyapps/fancybox';
import page from 'page';
import forms from 'forms';
import {
  disableBodyScroll,
  enableBodyScroll,
  clearAllBodyScrollLocks
} from 'body-scroll-lock';


import mainPage from 'index.page';
import ymap from 'ymap';


let app = {

  scrollToOffset: 200, // оффсет при скролле до элемента
  scrollToSpeed: 500, // скорость скролла

  init: function() {
    // read config
    if (typeof appConfig === 'object') {
      Object.keys(appConfig).forEach(key => {
        if (Object.prototype.hasOwnProperty.call(app, key)) {
          app[key] = appConfig[key];
        }
      });
    }

    app.currentID = 0;

    // Init page
    this.page = page;
    this.page.init.call(this);

    this.ymap = ymap;
    this.ymap.init.call(this);

    this.forms = forms;
    this.forms.init.call(this);

    // Init page

    this.mainPage = mainPage;
    this.mainPage.init.call(this);

    //window.jQuery = $;
    window.app = app;

    app.document.ready(() => {

      const presentForm = $('.js-main-form');

      var url = $(this).attr('action');

      presentForm.on('submit', function(e) {
        e.preventDefault();
        const _form = $(this);
        const url = _form.attr('action');
        const thnxModal = '#succes';
        let data = _form.serialize();

        $.ajax(url, {
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'
          },
          method: 'post',
          data: data,
          success: function(result) {
            if (result.status == 'success') {
              $.fancybox.close();
              $.fancybox.open({
                toolbar: false,
                smallBtn: true,
                touch: false,
                autoFocus: false,
                src: thnxModal,
                opts: {
                  afterLoad: function() {
                    let fancyboxSlide = document.querySelectorAll('.fancybox-slide');

                    fancyboxSlide.forEach(function(element) {
                      disableBodyScroll(element);
                    });
                  },
                  beforeClose: function() {
                    clearAllBodyScrollLocks();
                  },
                },
              }, {
                autoFocus: false,
                touch: false,
              }, );
            } else {
              console.log('error');
            }
          }

        });

        // _form.trigger('reset');
      });

      const $inputBet = document.querySelector('.js-input-bet');

      $inputBet.addEventListener('input', _.debounce(roundInput, 1000));

      function roundInput(event) {
        let inputNum = event.target.value.substring(0, event.target.value.length - 1);
        event.target.value = roundToMultiple(inputNum)
      }

      function roundToMultiple(num, multiple = 100) {
        let result = Math.round(num / multiple) * multiple;
        return result
      }

    });



    app.window.on('load', () => {
      $('[data-fancybox]').fancybox({
        toolbar: false,
        smallBtn: true,
        touch: false,
        autoFocus: false,

        afterLoad: function() {
          var fancyboxSlide = document.querySelectorAll(".fancybox-slide");
          fancyboxSlide.forEach(function(element) {
            // scrollLock.disablePageScroll(element);
            disableBodyScroll(element);

          });
        },
        beforeClose: function() {
          if ($('.fancybox-slide').length == 1) {
            // scrollLock.enablePageScroll();
            clearAllBodyScrollLocks();
          }
        },
      });

    });

    // this.document.on(app.resizeEventName, () => {
    // });

  },

};
app.init();